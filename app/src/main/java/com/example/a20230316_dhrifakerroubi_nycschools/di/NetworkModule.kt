package com.example.a20230316_dhrifakerroubi_nycschools.di

import com.example.a20230316_dhrifakerroubi_nycschools.data.Repository
import com.example.a20230316_dhrifakerroubi_nycschools.data.RepositoryImpl
import com.example.a20230316_dhrifakerroubi_nycschools.data.remote.SchoolApi
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {


    @Provides
    fun provideClient(
//        loggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient{
//        val loggingInterceptor = HttpLoggingInterceptor().apply {
//            level = Level.BODY
//        }
        return OkHttpClient.Builder()
        //    .addInterceptor( loggingInterceptor)
            .build()
    }

    @Provides
    fun provideRetrofit(
        client: OkHttpClient
    ): Retrofit{
        return Retrofit.Builder()
            .baseUrl(SchoolApi.BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .build()
    }

    @Provides
    fun provideSchoolApi(
        retrofit: Retrofit
    ):SchoolApi{
        return retrofit.create(SchoolApi::class.java)
    }

    @Provides
    fun provideRepository(
        apiDetails: SchoolApi
    ): Repository{
        return RepositoryImpl(apiDetails)
    }

}