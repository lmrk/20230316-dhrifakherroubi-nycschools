package com.example.a20230316_dhrifakerroubi_nycschools.ui.score

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import com.example.a20230316_dhrifakerroubi_nycschools.R
import com.example.a20230316_dhrifakerroubi_nycschools.databinding.ScoreFragmentBinding
import com.example.a20230316_dhrifakerroubi_nycschools.ui.SchoolViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class ScoreFragment: Fragment() {

    companion object {
        const val SCORE_KEY = "SCORE_KEY"
        fun newInstance(dbn: String): ScoreFragment {
            val fragment = ScoreFragment()
            val bundle = Bundle()
            bundle.putString(SCORE_KEY, dbn)
            fragment.arguments = bundle
            return fragment
        }
    }

    private var _binding: ScoreFragmentBinding? = null
    private val binding : ScoreFragmentBinding get() = _binding!!

//    @Inject lateinit var viewModel: SchoolViewModel
    private val viewModel: SchoolViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = ScoreFragmentBinding.inflate(layoutInflater)
        viewModel.getCurrentScore(arguments?.getString(SCORE_KEY)!!)
        configureObserver()
        return binding.root
    }

    private fun configureObserver() {
        viewModel.score.observe(viewLifecycleOwner) { score ->
            if (!score.isNullOrEmpty()) {
                binding.apply {
                    schoolName.text = score[0].school_name
                    scoreMath.text = score[0].sat_math_avg_score
                    scoreReading.text = score[0].sat_critical_reading_avg_score
                    scoreWriting.text = score[0].sat_writing_avg_score
                    scoreMath.visibility = View.VISIBLE
                    scoreReading.visibility = View.VISIBLE
                    scoreWriting.visibility = View.VISIBLE
                }
            } else {
                binding.apply {
                    schoolName.text = resources.getString(R.string.score_error)
                    scoreMath.visibility = View.INVISIBLE
                    scoreReading.visibility = View.INVISIBLE
                    scoreWriting.visibility = View.INVISIBLE
                }

            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.apply {
            scoreMath.visibility = View.INVISIBLE
            scoreReading.visibility = View.INVISIBLE
            scoreWriting.visibility = View.INVISIBLE
        }
        _binding = null
    }
}