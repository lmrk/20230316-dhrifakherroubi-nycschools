package com.example.a20230316_dhrifakerroubi_nycschools.ui.schools

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20230316_dhrifakerroubi_nycschools.R
import com.example.a20230316_dhrifakerroubi_nycschools.databinding.SchoolsListFragmentBinding
import com.example.a20230316_dhrifakerroubi_nycschools.ui.SchoolViewModel
import com.example.a20230316_dhrifakerroubi_nycschools.ui.score.ScoreFragment
import dagger.hilt.android.AndroidEntryPoint

private const val TAG = "SchoolsListFragment"

@AndroidEntryPoint
class SchoolsListFragment : Fragment() {
    lateinit var binding: SchoolsListFragmentBinding
//    private val viewModel by lazy {
//        ViewModelProvider(requireActivity())[SchoolViewModel::class.java]
//    }

    private val viewModel: SchoolViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = SchoolsListFragmentBinding.inflate(layoutInflater)
        configureObserver()
        return binding.root
    }

    private fun configureObserver() {
        viewModel.schools.observe(viewLifecycleOwner) {
            if (!it.isNullOrEmpty()) {
                binding.apply {
                    schoolList.adapter = SchoolsAdapter(it, ::showDetails)
                    schoolList.layoutManager = LinearLayoutManager(requireContext())
                }
            } else {
                binding.apply {
                    schoolErrorText.text = resources.getString(R.string.school_error)
                    schoolErrorText.visibility = View.VISIBLE
                }
            }
            viewModel.getSchools()
        }
    }

    private fun showDetails(dbn: String) {
        parentFragmentManager.beginTransaction()
            .replace(R.id.fragments_container_view, ScoreFragment.newInstance(dbn))
            .addToBackStack(null)
            .commit()
    }
}