package com.example.a20230316_dhrifakerroubi_nycschools.ui

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.a20230316_dhrifakerroubi_nycschools.data.Repository
import com.example.a20230316_dhrifakerroubi_nycschools.data.model.SchoolModel
import com.example.a20230316_dhrifakerroubi_nycschools.data.model.ScoreModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolViewModel @Inject constructor(private val repo: Repository): ViewModel() {
    private val TAG = "SchoolViewModel"
    private val _schools = MutableLiveData<List<SchoolModel>>()
    val schools: LiveData<List<SchoolModel>> get() = _schools

    private val _score = MutableLiveData<List<ScoreModel>>()
    val score: LiveData<List<ScoreModel>> get() = _score

    fun getSchools() {
        Log.d(TAG, "getSchools: start")
        CoroutineScope(Dispatchers.IO).launch {
            val response = repo.getSchools()
            if (response.isSuccessful) {
                Log.d(TAG, "getSchools: ${response.body()}")
                _schools.postValue(response.body()!!)
            } else {
                Log.d(TAG, "getSchools: fail")
                _schools.postValue(emptyList())
            }
        }
    }

    fun getCurrentScore(dbn: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = repo.getScore(dbn)
            if (response.isSuccessful) {
                _score.postValue(response.body()!!)
            } else {
                _score.postValue(emptyList())
            }
        }
    }
}